package com.thpa.a9019.kotlinimagefilters.Adapter

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.thpa.a9019.kotlinimagefilters.Interface.FilterListFragmentListenner
import com.thpa.a9019.kotlinimagefilters.R
import com.zomato.photofilters.utils.ThumbnailItem
import kotlinx.android.synthetic.main.thumbnail_list_item.view.*

class ThumnailAdapter(private val context: Context,
                      private val thumbnailItemList: MutableList<ThumbnailItem>,
                      private val listener: FilterListFragmentListenner) : RecyclerView.Adapter<ThumnailAdapter.MyViewHolder>() {
    private var selectedIndex = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(context).inflate(R.layout.thumbnail_list_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return thumbnailItemList.size
    }

    private val image: Bitmap?=null
    private val filter: Bitmap?=null

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val thumbNailItem = thumbnailItemList[position]
        holder.thumbNail.setImageBitmap(thumbNailItem.image)
        holder.thumbNail.setOnClickListener {
            listener.onFilterSelected(thumbNailItem.filter)
            selectedIndex = position
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var thumbNail: ImageView
        var filterName: TextView

        init {
            thumbNail = itemView.thumbnail
            filterName = itemView.filter_name
        }


    }
}