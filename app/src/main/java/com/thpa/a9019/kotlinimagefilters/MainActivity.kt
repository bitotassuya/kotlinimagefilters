package com.thpa.a9019.kotlinimagefilters

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.thpa.a9019.kotlinimagefilters.Adapter.ViewPagerAdapter
import com.thpa.a9019.kotlinimagefilters.Interface.EditImageFragmentListenner
import com.thpa.a9019.kotlinimagefilters.Interface.FilterListFragmentListenner
import com.thpa.a9019.kotlinimagefilters.Utils.BitmapUtils
import com.thpa.a9019.kotlinimagefilters.Utils.NonSwipeableViewPager
import com.zomato.photofilters.imageprocessors.Filter
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), FilterListFragmentListenner, EditImageFragmentListenner {
    val SELECT_GALLERY_PERMISSON = 1000
    override fun onBrightnessChanged(brightness: Int) {
        birgthnessFinal = brightness
        val myFilter = Filter()
        myFilter.addSubFilter(BrightnessSubFilter(brightness))
        image_Preview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)))

    }

    override fun onSaturationChanged(saturation: Float) {
        saturationFinal = saturation
        val myFilter = Filter()
        myFilter.addSubFilter(SaturationSubfilter(saturation))
        image_Preview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)))
    }

    override fun onConstrantChanged(constrant: Float) {
        contrasFinal = constrant
        val myFilter = Filter()
        myFilter.addSubFilter(ContrastSubFilter(constrant))
        image_Preview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)))
    }

    override fun onEditStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCompelte() {
        val bitmap = filteredImage.copy(Bitmap.Config.ARGB_8888, true)
        val myFilter = Filter()
        myFilter.addSubFilter(BrightnessSubFilter(birgthnessFinal))
        myFilter.addSubFilter(SaturationSubfilter(saturationFinal))
        myFilter.addSubFilter(ContrastSubFilter(contrasFinal))
        finalImage = myFilter.processFilter(bitmap)

    }

    override fun onFilterSelected(filter: Filter) {
        resetControl()
        filteredImage = originalImage!!.copy(Bitmap.Config.ARGB_8888, true)
        image_Preview.setImageBitmap(filter.processFilter(filteredImage))
        finalImage = filteredImage.copy(Bitmap.Config.ARGB_8888, false)

    }

    private fun resetControl() {
        if (editImageFragment != null)
            editImageFragment.ResetControls()
        birgthnessFinal = 0
        saturationFinal = 1.0f
        contrasFinal = 1.0f

    }

    internal var originalImage: Bitmap? = null
    internal lateinit var filteredImage: Bitmap
    internal lateinit var finalImage: Bitmap

    internal lateinit var filterListFragment: FilterListFragment
    internal lateinit var editImageFragment: EditImageFragment

    internal var birgthnessFinal = 0
    internal var saturationFinal = 1.0f
    internal var contrasFinal = 1.0f

    object Main {
        val IMAGE_NAME = "Non.jpg"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //set Toobat
        setSupportActionBar(toobar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Instagram Filter"

        loadImage()
        setupViewPager(view_Pager)
        tab.setupWithViewPager(view_Pager)

    }

    private fun setupViewPager(view_Pager: NonSwipeableViewPager?) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        //add filter list Fragment
        filterListFragment = FilterListFragment()
        filterListFragment.setListenner(this)

        //set edit image Fragment
        editImageFragment = EditImageFragment()
        editImageFragment.setListener(this)

        adapter.addFragment(filterListFragment, "FILTER")
        adapter.addFragment(editImageFragment, "EDIT")
        view_Pager!!.adapter = adapter

    }


    private fun loadImage() {
        originalImage = BitmapUtils.getBitmapFromAssets(this, Main.IMAGE_NAME, 300, 300)
        filteredImage = originalImage!!.copy(Bitmap.Config.ARGB_8888, true)
        finalImage = originalImage!!.copy(Bitmap.Config.ARGB_8888, true)
        image_Preview.setImageBitmap(originalImage)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int = item!!.itemId
        if (id == R.id.action_open) {
            openImageFromGallery()
            return true
        } else if (id == R.id.action_save) {
            saveImageToGallery()
            return true

        }
        return super.onOptionsItemSelected(item)
    }

    private fun openImageFromGallery() {
        //we will Dexter to request runtime permission and process
        val withListener = Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            val intent = Intent(Intent.ACTION_PICK)
                            intent.type = "image/*"
                            startActivityForResult(intent, SELECT_GALLERY_PERMISSON)
                        } else Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT).show()
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token!!.continuePermissionRequest()
                    }

                }).check()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_GALLERY_PERMISSON) {
            val bitmap = BitmapUtils.getBitmapFromGallery(this, data!!.data!!, 800, 800)
            //clear bitmap memory
            originalImage!!.recycle()
            finalImage!!.recycle()
            filteredImage.recycle()

            originalImage = bitmap.copy(Bitmap.Config.ARGB_8888, true)
            filteredImage = originalImage!!.copy(Bitmap.Config.ARGB_8888, true)
            finalImage = originalImage!!.copy(Bitmap.Config.ARGB_8888, true)

            bitmap.recycle()

            //render select image thumb
            filterListFragment.displayImage(bitmap)
        }
    }

    private fun saveImageToGallery() {
        Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            val path = BitmapUtils.insertImage(contentResolver,
                                    finalImage,
                                    System.currentTimeMillis().toString() + "_profile.jpg"
                                    , "")
                            if (!TextUtils.isEmpty(path)) {
                                val snackbar = Snackbar.make(coordinator, "Image saved to gallery", Snackbar.LENGTH_LONG)
                                        .setAction("OPEN", {
                                            openImage(path)
                                        })
                                snackbar.show()
                            } else {
                                val snackbar = Snackbar.make(coordinator, "Unable to save image", Snackbar.LENGTH_LONG)
                                snackbar.show()
                            }
                        }

                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT).show()
                    }

                }).check()
    }

    private fun openImage(path: String?) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(Uri.parse(path), "image/")
        startActivity(intent)

    }


}
