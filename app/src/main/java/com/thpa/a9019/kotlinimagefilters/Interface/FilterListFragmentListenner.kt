package com.thpa.a9019.kotlinimagefilters.Interface

import com.zomato.photofilters.imageprocessors.Filter

interface FilterListFragmentListenner {
    fun onFilterSelected(filter: Filter)
}