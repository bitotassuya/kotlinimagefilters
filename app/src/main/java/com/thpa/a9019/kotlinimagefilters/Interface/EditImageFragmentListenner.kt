package com.thpa.a9019.kotlinimagefilters.Interface

interface EditImageFragmentListenner {
    fun onBrightnessChanged(brightness:Int)
    fun onSaturationChanged(saturation:Float)
    fun onConstrantChanged(constrant:Float)
    fun onEditStarted()
    fun onCompelte()
}