package com.thpa.a9019.kotlinimagefilters


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thpa.a9019.kotlinimagefilters.Adapter.ThumnailAdapter
import com.thpa.a9019.kotlinimagefilters.Interface.FilterListFragmentListenner
import com.thpa.a9019.kotlinimagefilters.Utils.BitmapUtils
import com.thpa.a9019.kotlinimagefilters.Utils.SpaceItemDecoration
import com.zomato.photofilters.FilterPack
import com.zomato.photofilters.imageprocessors.Filter
import com.zomato.photofilters.utils.ThumbnailItem
import com.zomato.photofilters.utils.ThumbnailsManager
import kotlinx.android.synthetic.main.fragment_filter_list.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FilterListFragment : Fragment(), FilterListFragmentListenner {
    internal var listenner: FilterListFragmentListenner? = null
    internal lateinit var adapter: ThumnailAdapter
    internal lateinit var thumbnailItemList: MutableList<ThumbnailItem>


    fun setListenner(listFragmentListenner: FilterListFragmentListenner) {
        this.listenner = listFragmentListenner
    }

    override fun onFilterSelected(filter: Filter) {
        if (listenner != null) {
            listenner!!.onFilterSelected(filter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_filter_list, container, false)
        thumbnailItemList = ArrayList()
        adapter = ThumnailAdapter(activity!!, thumbnailItemList, this)
        recycler_view.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recycler_view.itemAnimator = DefaultItemAnimator()
        var space: Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, resources.displayMetrics)
                .toInt()
        recycler_view.addItemDecoration(SpaceItemDecoration(space))
        recycler_view.adapter = adapter
        displayImage(null)
        return view
    }

     fun displayImage(bitmap: Bitmap?) {

        val r = Runnable {
            val thumbImage: Bitmap?
            if (bitmap == null) {
                thumbImage = BitmapUtils.getBitmapFromAssets(activity!!, MainActivity.Main.IMAGE_NAME, 100, 100)

            } else {
                thumbImage = Bitmap.createScaledBitmap(bitmap, 100, 100,false)
            }
            if (thumbImage == null) {
                return@Runnable
            }
            ThumbnailsManager.clearThumbs()
            thumbnailItemList.clear()

            //add normal bitmap first
            val thumbnaiilItem = ThumbnailItem()
            thumbnaiilItem.image = thumbImage
            thumbnaiilItem.filterName = "Normal"
            ThumbnailsManager.addThumb(thumbnaiilItem)

            //add filter pack
            val filters = FilterPack.getFilterPack(activity!!)
            for (filter in filters)
            {
                val item = ThumbnailItem()
                item.image = thumbImage
                item.filter = filter
                item.filterName = filter.name
                ThumbnailsManager.addThumb(item)

            }
            thumbnailItemList.addAll(ThumbnailsManager.processThumbs(activity))
            activity!!.runOnUiThread {
                adapter.notifyDataSetChanged()
            }
        }
        Thread(r).start()
    }


}
