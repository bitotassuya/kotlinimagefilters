package com.thpa.a9019.kotlinimagefilters


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.thpa.a9019.kotlinimagefilters.Interface.EditImageFragmentListenner
import kotlinx.android.synthetic.main.fragment_edit_image.*



private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class EditImageFragment : Fragment()
        , SeekBar.OnSeekBarChangeListener {
    private var listenner: EditImageFragmentListenner? = null

    fun ResetControls() {
        seekbar_brigth.progress = 100
        seekbar_constrant.progress = 0
        seekbar_saturation.progress = 0
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        var progress = progress
        if (listenner != null) {
            if (seekBar!!.id == R.id.seekbar_brigth) {
                listenner!!.onBrightnessChanged(progress - 100)
            } else if (seekBar!!.id == R.id.seekbar_constrant) {
                progress += 10
                val floatVal: Float = .10f * progress
                listenner!!.onConstrantChanged(floatVal)
            } else if (seekBar!!.id == R.id.seekbar_saturation) {
                val floatVal: Float = .10f * progress
                listenner!!.onSaturationChanged(floatVal)
            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        if (listenner != null)
            listenner!!.onEditStarted()
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        if (listenner != null)
            listenner!!.onEditStarted()
    }


    fun setListener(listener: EditImageFragmentListenner) {
        this.listenner = listenner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_edit_image, container, false)
        seekbar_brigth.max = 200
        seekbar_brigth.progress = 100

        seekbar_constrant.max = 20
        seekbar_constrant.progress = 0

        seekbar_saturation.max = 30
        seekbar_saturation.progress = 10

        seekbar_brigth.setOnSeekBarChangeListener(this)
        seekbar_constrant.setOnSeekBarChangeListener(this)
        seekbar_saturation.setOnSeekBarChangeListener(this)
        return view
    }


}
